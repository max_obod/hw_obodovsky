from random import randint


def my_func_game():
    print("Try to guess a number between 1 and 100!")
    guess = 0
    counter = 5
    number = randint(1, 101)
    while guess != number:
        print("You have", counter, "attempt")
        counter -= 1
        try:
            guess = int(input("Enter the number: "))
        except ValueError:
            print("Enter the number!!!1!")
            if number - guess > 10 or number - guess < -10:
                print("Cold.")
            elif 10 >= number - guess >= 5 or -5 >= number - guess >= -10:
                print("Warmly")
            elif 4 >= number - guess >= 1 or -1 >= number - guess >= -4:
                print("Hot")
            elif number == guess:
                print("You got it!!!!!!")
                break
            if counter == 0:
                print("Looser")
                break


def again():
    answer = input("Play again? Y/N")
    while answer == "Y":
        my_func_game()
        answer = input("Play again? Y/N")
        if answer != "Y":
            break
    print("Thx 4 playing")
