# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
# и возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py) попросить
# пользователя ввести с клавиатуры строку и вывести ее на экран. Используя импортированную из lib.py функцию спросить
# у пользователя, хочет ли он повторить операцию (Y/N). Повторять пока пользователь отвечает Y и прекратить когда
# пользователь скажет N.

from lib import answer

print("Want to display text?")
while answer():
    temp_output = input("Enter the string: ")
    print("Output:", temp_output)
    print("let's repeat")

# - Модифицируем ДЗ2. Напишите с помощью функций!. Помните о Single Respinsibility! Попросить ввести свой возраст (
# можно использовать константу или input()). Пользователь ввел значение возраста [year number] а на место [year
# string] нужно поставить правильный падеж существительного "год", который зависит от значения [year number].
# - если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст и тд.) - вывести “не
# понимаю”
# - если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# - если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# - если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# - в любом другоm  случае - вывести “Оденьте маску, вам же [year number] [year string]!”
#
# Например:
# - Тебе 1 год, где твои мама и папа?
# - Оденьте маску, вам же 23 года!
# - Вам уже 68 лет, вы в зоне риска!



year_number = int(input("How old are you?"))
year_string = ""
if year_number in [1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111]:
    year_string = "Год"
elif year_number in [2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82,
                     83, 84, 92, 93, 94, 102, 103, 104]:
    year_string = "Года"
else:
    year_string = "Лет"


def kids(year_number):
    if 0 < year_number < 7:
        return True


def teenager(year_number):
    if 7 <= year_number < 18:
        return True


def old(year_number):
    if 65 <= year_number:
        return True


def adults(year_number):
    if 18 <= year_number < 65:
        return True


def impossible(year_number):
    if year_number < 0 or year_number > 125 or not year_number.isdigit():
        return True

def my_func(year_number):
    if kids(year_number):
        return f"Тебе {year_number} {year_string}, где твои мама и папа?"
    if teenager(year_number):
        return f"Тебе {year_number} {year_string}, а мы не продаем сигареты несовершеннолетним"
    if old(year_number):
        return f"Вам уже {year_number} {year_string}, вы в зоне риска"
    if adults(year_number):
        return f"Оденьте маску, вам же {year_number} {year_string}!"
    if impossible(year_number):
        return "Не понимаю."

print(my_func(year_number))
