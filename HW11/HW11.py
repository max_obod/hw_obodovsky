# Напишите декоратор, который бы сообщал о времени выполнения функции

import time


def time_of_func(function):
    def wrapped(*args):
        start_time = time.perf_counter()
        res = function(*args)
        timer = time.perf_counter() - start_time
        print("Time: ", timer, "sec")
        return res

    return wrapped


@time_of_func
def some_func(arg1, arg2):
    res = (arg1 * arg2) ** (arg2 / arg1)
    return res


print(some_func(2, 4))


# Напишите переметризованный декоратор, который бы выводил время выполнения функции в милисекундах или секундах или
# минутах (как выводить - определяет параметр декоратора)

def parametrized_decorator(dec_args):
    def wrapper_outer(func):
        def wrapper_inner(*args):
            start_time = time.perf_counter()
            res = func(*args)
            timer = time.perf_counter() - start_time
            if dec_args == "min":
                timer = timer / 60
                print("Time: ", timer, "min")
            elif dec_args == "msec":
                timer = timer * 1000
                print("Time: ", timer, "msec")
            elif dec_args == "sec":
                timer = time.perf_counter()
                print("Time: ", timer, "sec")
            else:
                raise TypeError
            return res

        return wrapper_inner

    return wrapper_outer


@parametrized_decorator("min")
def func(arg1, arg2):
    res = (arg1 * arg2) / (arg2 + arg1) ** (arg2 - arg1)
    return res


print(func(2, 54))
