# Напишите класс, отвечающий за животное. Реализуйте в классе атрибуты : количество лап, издаваемый звук, кличка.
# Реализуйте в классе метод "издать звук". Количество лап и звук задается при инициализации и имеет ограничения
# (ограничения придумайте сами). Кличка дается после инициализции. Создайте несколько обьектов класса, напр кошка,
# собака, птица, и тд.,

class Animal:
    def __init__(self, paws, voice, name):
        self.voice = voice
        self.paws = paws
        self.name = name
        if paws > 4:
            print("Мы не работаем с такими животными.")
            self.paws = 4


    def make_sound(self):
        return f"Этот зверь издает звук: {self.voice}"

    def description(self):
        return f"У кого {self.paws} лапы, кто издает звук: {self.voice}, и зовут его {self.name}?"


tac = Animal(4, "мяу", "Семен" )
print(tac.description())
god = Animal(4, "Гав", " Барбос")
print(god.description())
print(god.make_sound())
kcoc = Animal(2, "Ку-ка-ре-ку", "Тайсон")
print(kcoc.description())


