# Создайте класс "Транспортное средство" и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль".
# Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты для "Самолет", "Автомобиль", "Корабль".
# В наследниках реализуйте характерные для них атрибуты и методы

class vehicle:
    max_speed = 0
    n_of_passengers = 0


class airplane(vehicle):
    runway_length = 0
    number_of_wings = 0

    def __init__(self, new_max_speed, new_n_of_passengers, new_runway_length, new_number_of_wings):
        self.max_speed = new_max_speed
        self.n_of_passengers = new_n_of_passengers
        self.runway_length = new_runway_length
        self.number_of_wings = new_number_of_wings

    def airplane_info(self):
        return f"Этот самолет разгонается до {self.max_speed} км/ч, в нем могут находиться {self.n_of_passengers}" \
               f" пассажира, длинна взлетной полосы - {self.runway_length}м, кол-во крыльев - {self.number_of_wings}"



class car(vehicle):
    engines_type = ""
    type_of_drive = ""

    def __init__(self, new_max_speed, new_n_of_passengers, new_engines_type, new_type_of_drive):
        self.max_speed = new_max_speed
        self.n_of_passengers = new_n_of_passengers
        self.engines_type = new_engines_type
        self.type_of_drive = new_type_of_drive

    def car_info(self):
        return f"Эта машина разгоняется до {self.max_speed}км/ч, она {self.n_of_passengers}-ух(ех) местная, " \
               f"В ней стоит {self.engines_type} двигатель и {self.type_of_drive} привод"


class vessel(vehicle):
    mover = ""
    sailing_area = ""

    def __init__(self, new_max_speed, new_n_of_passengers, new_mover, new_sailing_area):
        self.max_speed = new_max_speed
        self.n_of_passengers = new_n_of_passengers
        self.mover = new_mover
        self.sailing_area = new_sailing_area

    def vessel_info(self):
        return f"Это судно разногняется до {self.max_speed} узлов, Тут могут проживать {self.n_of_passengers} пассажира," \
               f" для движения судно использует {self.mover}, и ходит по {self.sailing_area}"


a1 = airplane(20000, 1000, 100, 4)
print(a1.airplane_info())
c1 = car(260, 4, "fuel", "front-wheel drive")
print(c1.car_info())
v1 = vessel(40, 50, "Sail", "Ocean")
print(v1.vessel_info())