# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# - в любом другом случае вернуть кортеж (tuple) из аргументов

def my_func(arg1, arg2):
    if type(arg1) and type(arg2) == int:
        res = arg1 * arg2
    elif type(arg1) and type(arg2) == str:
        res = arg1 + " " + arg2
    elif type(arg1) == str and type(arg2) != str:
        res = {
            arg1: arg2
        }
    else:
        res = (arg1, arg2)
    return res


result = my_func(arg1=2, arg2=3)
print(result)
print(type(result))

# Пишем игру ) Программа выбирает из диапазона чисел (1-100) случайное число и предлагает пользователю его угадать.
# Пользователь вводит число. Если пользователь не угадал - предлагает пользователю угадать еще раз, пока он не
# угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N). Если Y - повторить игру. N - закончить

from random import randint


def my_Game():
    def my_func_game():
        print("Try to guess a number between 1 and 100!")
        guess = 0
        number = randint(1, 101)
        while guess != number:
            try:
                guess = int(input("Enter the number: "))
                if guess == number:
                    print("You got it!1!!!")
                elif guess > number:
                    print("You guessed too high")
                else:
                    print("You guessed too low")
            except ValueError:
                print("Enter the number!!!1!")

    my_func_game()

    answer = input("Play again? Y/N")
    while answer == "Y":
        my_func_game()
        answer = input("Play again? Y/N")
        if answer != "Y":
            break
    print("Thx 4 playing")


my_Game()


# Пользователь вводит строку произвольной длины. Написать функцию, которая должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - список слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:
# {
# "0": количество пробелов в строке
# "1": list слов из одной буквы
# "2": list слов из двух букв
# "3": list слов из трех букв
# и т.д ...
# "punctuation" : tuple уникальных знаков препинания
# }

import string

line = input("Enter the line: ")


def dict_func():
    line_split = line.split()
    line_split.sort(key=len)
    print(line_split)
    a = tuple()
    my_dict = {
        0: line.count(" "),
        "punctuation": a
    }

    for word in line_split:
        temp_tuple = []
        count = len(word)
        temp_list = []
        for word in line_split:
            for symbol in word:
                if symbol in string.punctuation:
                    temp_tuple.append(symbol)
                    tuple1 = tuple(temp_tuple)
                    my_dict["punctuation"] = tuple1
            if count == len(word):
                temp_list.append(word)
        my_dict[count] = temp_list

    print(my_dict)


dict_func()
