# Доработайте классы Point и Line из занятия. Обеспечьте передачу в атрибуты x и y класса Point только чисел,
# а в атрибуты start_point и end_point класса Line только обьектов класса Point

class Point:
    x = 0
    y = 0

    def __init__(self, new_x, new_y):
        self._x = new_x
        self._y = new_y

    @property
    def new_x(self):
        return self._x

    @new_x.setter
    def new_x(self, x):
        if not isinstance(self.new_x, (int, float)):
            raise TypeError
        self._x = x

    @property
    def new_y(self):
        return self._y

    @new_y.setter
    def new_y(self, y):
        if not isinstance(self.new_y, (int, float)):
            raise TypeError
        self._y = y


class Line:
    _start_point = None
    _end_point = None

    def __init__(self, start_point, end_point):
        self.start_point = start_point
        self.end_point = end_point

    @property
    def start_point(self):
        return self._start_point

    @start_point.setter
    def start_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._start_point = value

    @property
    def end_point(self):
        return self._end_point

    @end_point.setter
    def end_point(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._end_point = value

    @property
    def length(self):
        res = ((self._start_point._x - self._end_point._x) ** 2 + (
                    self._start_point._y - self._end_point._y) ** 2) ** 0.5
        return res


line1 = Line(Point(10, 2), Point(2, 4))
print(line1._end_point.__class__)
print(line1.length)
print(type(line1.length))


# Реализуйте в классе Point механизм сложения таким образом, чтобы при сложении двух точек получался обьект класса Line

class Point:
    x = 0
    y = 0

    def __init__(self, new_x, new_y):
        self._x = new_x
        self._y = new_y

    @property
    def new_x(self):
        return self._x

    @new_x.setter
    def new_x(self, x):
        if not isinstance(self.new_x, (int, float)):
            raise TypeError
        self._x = x

    @property
    def new_y(self):
        return self._y

    @new_y.setter
    def new_y(self, y):
        if not isinstance(self.new_y, (int, float)):
            raise TypeError
        self._y = y


    def __add__(self, other):
        res = Line(Point(self._x, self._y), Point(other.x, other.y))
        return res


class Line:
    _start_point = None
    _end_point = None

    def __init__(self, start_point, end_point):
        self.start_point = start_point
        self.end_point = end_point


    @property
    def length(self):
        res = ((self.start_point._x - self.end_point._x) ** 2 + (
                    self.start_point._y - self.end_point._y) ** 2) ** 0.5
        return res



line1 = Point(10, 2) + Point(2, 3)
print(line1.__class__)
print(line1.length)
