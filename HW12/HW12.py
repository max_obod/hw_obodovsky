import requests
import datetime


# Решил сделать 2 задания в одном

class Exchange:
    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    main_currency = "UAH"
    additional_currency = "USD"
    date = "20011212"

    def __init__(self, cur=None, new_date=None):
        if cur is not None:
            self.additional_currency = cur
        if new_date is not None:
            self.date = new_date

    @property
    def additional_currency(self):
        return self._additional_currency

    @additional_currency.setter
    def additional_currency(self, value):
        if not isinstance(value(str)):
            raise TypeError
        elif len(value) != 3:
            raise "Enter letter code(USD, UAH....)"
        value = value.upper()
        self._additional_currency = value

    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, value):
        if not isinstance(value(str)):
            raise TypeError
        elif len(value) != 8:
            raise "Enter date like in example: YYYYMMDD)"
        self._date = value

    def connection(func):
        def _wrapper(*args, **kwargs):
            url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
            try:
                res = requests.get(url)
            except:
                print("Some exception")
            else:
                if 300 > res.status_code >= 200 and res.headers["Content-Type"] == "application/json":
                    pass
            result = func(*args, **kwargs)
            return result

        return _wrapper

    @connection
    def currency_today(self, main_cur=main_currency):
        date = datetime.datetime.now()
        today = f"{date.year}{date.month}{date.day}"
        self.url = (f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={today}&json')
        res = requests.get(self.url)
        json_res = res.json()
        counter = 0
        cur_list = []
        actual_date = json_res[0]["exchangedate"]
        with open("exchange", "w") as file:
            file.write(actual_date + "\n")
            for i in json_res:
                counter += 1
                rate = i["rate"]
                cur = i["cc"]
                answer = f"{counter}. {cur} to {main_cur}: {rate}"
                cur_list.append(answer)
            file.writelines(cur_list)
        return "exchange in 'exchange'"

    @connection
    def user_request(self):
        user_cur = self.additional_currency
        user_date = self.date
        self.url = (f'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={user_date}&json')
        res = requests.get(self.url)
        json_res = res.json()
        list_cur = []
        for i in json_res:
            list_cur.append(i["cc"])
            if i["cc"] == user_cur:
                return f"{json_res[0]['exchange_date']} rate of {user_cur} to UAH is: {i['rate']}"
            if user_cur not in list_cur:
                return "Try again"


a = Exchange
print(a.currency_today())
b = Exchange("eur", "20011212")
print(b.user_request())
print(b.currency_today())


def user_input():
    print("Введите абривиатурное название валюты(USD, UAH,...) и дату в формате YYYYMMDD")
    input_cur = input("Валюта: ")
    input_date = input("Дата: ")
    a = Exchange(input_cur, input_date)
    return a.user_request

print(user_input())